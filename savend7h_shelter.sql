-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Янв 11 2022 г., 22:16
-- Версия сервера: 5.7.21-20-beget-5.7.21-20-1-log
-- Версия PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `savend7h_shelter`
--

-- --------------------------------------------------------

--
-- Структура таблицы `pets`
--
-- Создание: Янв 02 2022 г., 23:30
-- Последнее обновление: Янв 03 2022 г., 02:07
--

DROP TABLE IF EXISTS `pets`;
CREATE TABLE `pets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `type_id` int(10) UNSIGNED NOT NULL,
  `pol` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pets`
--

INSERT INTO `pets` (`id`, `name`, `description`, `type_id`, `pol`, `created_at`, `updated_at`) VALUES
(4, 'Дина', 'Небольшая молоденькая девочка Дина, абсолютно контактная, ласковая, игривая. Скорее всего когда-то Дина была домашней, потому что ошейник и поводок ее совершенно не удивили. Нужно совсем немного времени, чтобы завоевать ее доверие. И потом она постарается сделать все, чтобы не упустить вас из виду. Даже играть с подружками не будет, если вы вдруг куда-то исчезнете из ее поля зрения. Посмотрите, какая забавная мордашка с ушками-локаторами)) эти локаторы всегда настроены на вас. А вот сама Дина редко сидит на одном месте, она все время в движении, все время в поисках новых впечатлений. Она обязательно полюбится семье, ведущей активный образ жизни и мечтающей о маленьком пушистом друге.\r\nДина ниже колена, здорова, стерилизована, привита, готова к встрече со своей семьей.', 1, 'F', '2022-01-02 22:18:12', '2022-01-02 23:07:12');

-- --------------------------------------------------------

--
-- Структура таблицы `types`
--
-- Создание: Янв 02 2022 г., 22:49
-- Последнее обновление: Янв 03 2022 г., 02:04
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `types`
--

INSERT INTO `types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Собака', '2022-01-02 18:01:04', '2022-01-02 20:50:18'),
(3, 'Кот', '2022-01-02 18:21:21', '2022-01-02 18:21:21');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Индексы таблицы `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `pets`
--
ALTER TABLE `pets`
  ADD CONSTRAINT `pets_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
