<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>{{ $name }}</title>
    <style>body { font-family: DejaVu Sans }</style>
</head>
<body>
<h3 class="card-title mb-4">{{ $name }}</h3>
<p class="card-text">
    <b>Тип:</b> {{ $type }} <br>
    <b>Пол:</b> @if ($pol=='F') Девочка @else Мальчик @endif <br>
    {!! nl2br(e($description)) !!}
</p>
</body>
</html>