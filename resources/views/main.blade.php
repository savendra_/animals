@extends('layouts.page')

@section('content')
        <div class="row d-flex justify-content-center" style="padding-top:20vh">
            <div class="col-lg-4">
                <a href="{{route('types.index')}}">
                    <div class="card card-bl py-5 px-3">
                        <h5 class="text-center">Тип животных</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="{{route('pets.index')}}">
                    <div class="card card-bl py-5 px-3">
                        <h5 class="text-center">Список поступивших животных</h5>
                    </div>
                </a>
            </div>
        </div>

@endsection

