<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use App\Models\Type;
use Flash;


/**
 * Class TypeController
 * @package App\Http\Controllers
 */
class TypeController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

       // phpinfo();
      $types = Type::orderBy('name')->get();
      return view('types.index', compact('types'));
    }

    /**
     * @return mixed
     */
    public function create()
    {

        return view('types.create');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $type = new Type;
        $type->name = $request->name;
        $type->save();

        return redirect(route('types.index'))->with('message', 'Тип добавлен!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $type = Type::findOrFail($id);
        return view('types.edit', compact('type'));
    }


    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $type = Type::findOrFail($id);
        $type->name = $request->name;
        $type->update();

        return redirect(route('types.index'))->with('message', 'Тип был изменен!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $type = Type::findOrFail($id);

        $type->destroy($id);
        return redirect(route('types.index'))->with('message', 'Тип был удален!');
    }
}