@extends('layouts.page')

@section('content')

    <div class="py-5 d-flex justify-content-between">
        <h4>Список поступивших животных </h4>
        <a href="{{route('pets.create')}}" class="btn btn-outline-secondary"> <i class="las la-plus"></i> Добавить</a>
    </div>
    <div class="mb-5">
        <a href="/"><i class="las la-arrow-circle-left"></i>Вернуться назад</a>
    </div>
    @if (session('message'))
        <div class="alert alert-success my-3 px-2">
            {{ session('message') }}
        </div>
    @endif

    @if (count($pets)>0)
        <div class="card p-4">
            <table class="table">

                <tbody>

                @foreach ($pets as $pet)
                    <tr>
                        <td>{{ $pet->name }}</td>
                        <td style="width:250px">
                            <a class="btn btn-outline-primary" href="{{ route('pets.show', $pet->id) }}" title="Просмотр"><i class="las la-eye"></i></a>
                            <a class="btn btn-outline-warning" href="/create-pdf/{{$pet->id }}" title="Печать" target="_blank"><i class="las la-print"></i></a>
                            <a class="btn btn-outline-success" href="{{ route('pets.edit', $pet->id) }}" title="Изменить"><i class="las la-edit"></i></a>
                            <form method="POST" action="{{ route('pets.destroy', $pet->id) }}" style="display:inline-block;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button onclick="return confirm('Вы уверены, что хотите удалить?')" type="submit" class="btn btn-outline-danger" title="Удалить" ><i class='las la-trash'></i> </button>

                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection
