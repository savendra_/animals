@extends('layouts.page')

@section('content')

    <div class="py-5">
        <h4>Добавление нового животного </h4>
    </div>
    
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card py-5 px-5">
                @if (count($types)>0)
                    {!! Form::open(['route' => 'pets.store', 'method' => 'POST']) !!}
                    <div class="mb-3">
                        <label  class="form-label" for="name">Имя<span class="text-danger">*</span></label>
                        <?php  if ($errors->has('name')) $erName = true; ?>
                        {!!  Form::text('name', null, ['class' =>   isset($erName) ? 'form-control is-invalid ' : 'form-control', 'id' => 'name']) !!}
                        @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label  class="form-label" for="type">Тип<span class="text-danger">*</span></label>
                        <?php  if ($errors->has('type_id')) $erType= true; ?>
                        {!!  Form::select('type_id', $types, null, ['placeholder' => 'Выберите значение', 'class' =>   isset($erType) ? 'form-select is-invalid ' : 'form-select', 'id' => 'type']) !!}
                        @if ($errors->has('type_id'))
                            <span class="text-danger">{{ $errors->first('type_id') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <div class="form-check form-check-inline pe-5">
                            <input class="form-check-input" type="radio" name="pol" value="F" id="flexRadioDefault1" checked>
                            <label class="form-check-label" for="flexRadioDefault1">
                                Девочка
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="pol" value="M" id="flexRadioDefault2" >
                            <label class="form-check-label" for="flexRadioDefault2">
                                Мальчик
                            </label>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label  class="form-label" for="description">Описание</label>
                        {!!  Form::textarea('description', null, ['class' =>   'form-control', 'id' => 'description']) !!}

                    </div>
                    <div class="text-center">
                        <a href="{{route('pets.index')}}" class="btn btn-light">Отменить</a>
                        <button type="submit" class="btn btn-secondary">Добавить</button>
                    </div>
                    {!! Form::close() !!}
                @else
                    Отсутствуют типы. <a href="{{ route('types.create') }}">Добавьте</a> хотя бы один тип.
                @endif

            </div>
        </div>
    </div>

@endsection
