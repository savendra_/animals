@extends('layouts.page')

@section('content')

    <div class="py-5">
        <h4>Добавление нового типа животных </h4>
    </div>
    
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card py-5 px-5">
                {!! Form::open(['route' => 'types.store', 'method' => 'POST']) !!}
                <div class="mb-3">
                    <label  class="form-label" for="name">Название<span class="text-danger">*</span></label>
                    <?php  if ($errors->has('name')) $erName = true; ?>
                    {!!  Form::text('name', null, ['class' =>   isset($erName) ? 'form-control is-invalid ' : 'form-control', 'id' => 'name']) !!}
                    @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>
                <div class="text-center">
                    <a href="{{route('types.index')}}" class="btn btn-light">Отменить</a>
                   <button type="submit" class="btn btn-secondary">Добавить</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
