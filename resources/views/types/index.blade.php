@extends('layouts.page')

@section('content')

    <div class="py-5 d-flex justify-content-between">
        <h4>Типы животных </h4>
        <a href="{{route('types.create')}}" class="btn btn-outline-secondary"> <i class="las la-plus"></i> Добавить</a>
    </div>
    <div class="mb-5">
        <a href="/"><i class="las la-arrow-circle-left"></i>Вернуться назад</a>
    </div>
    @if (session('message'))
        <div class="alert alert-success my-3 px-2">
            {{ session('message') }}
        </div>
    @endif

    @if (count($types)>0)
        <div class="card p-4">
            <table class="table">

                <tbody>

                @foreach ($types as $type)
                    <tr>
                        <td>{{ $type->name }}</td>
                        <td style="width:150px">
                            <a class="btn btn-outline-success" href="{{ route('types.edit', $type->id) }}" title="Изменить"><i class="las la-edit"></i></a>
                            <form method="POST" action="{{ route('types.destroy', $type->id) }}" style="display:inline-block;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button onclick="return confirm('Вы уверены, что хотите удалить?При удалении типа удалятся и все связанные животные!')" type="submit" class="btn btn-outline-danger" title="Удалить" ><i class='las la-trash'></i> </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection
