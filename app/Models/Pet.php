<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Type;

class Pet extends Model
{
    use  HasFactory ;


    protected $fillable = [
        'name',
        'description',
        'pol'
    ];


    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function type()
    {
        return $this->belongsTo(Type::class);
    }
}
