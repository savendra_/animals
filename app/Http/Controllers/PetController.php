<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use App\Models\Type;
use App\Models\Pet;
use Flash;
use PDF;

/**
 * Class PetController
 * @package App\Http\Controllers
 */
class PetController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $pets = Pet::orderBy('name')->get();
        return view('pets.index', compact('pets'));
    }

    /**
     * @return mixed
     */
    public function create()
    {

        $types = Type::orderBy('name')->get()->pluck('name', 'id');
        return view('pets.create', compact('types'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'type_id' => 'required',
        ]);

        $pet = new Pet;
        $pet->name = $request->name;
        $pet->type_id = $request->type_id;
        $pet->pol = $request->pol;
        $pet->description = $request->description;
        $pet->save();

        return redirect(route('pets.index'))->with('message', 'Животное добавлено!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $pet = Pet::findOrFail($id);
        $types = Type::orderBy('name')->get()->pluck('name', 'id');
        return view('pets.edit', compact('pet', 'types'));
    }


    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'type_id' => 'required',
        ]);
        $pet = Pet::findOrFail($id);
        $pet->name = $request->name;
        $pet->type_id = $request->type_id;
        $pet->pol = $request->pol;
        $pet->description = $request->description;
        $pet->update();

        return redirect(route('pets.index'))->with('message', 'Животное было изменено!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $pet = Pet::findOrFail($id);
        return view('pets.show', compact('pet'));
    }
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $pet = Pet::findOrFail($id);

        $pet->destroy($id);
        return redirect(route('pets.index'))->with('message', 'Животное было удалено!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function pdf($id)
    {
        $pet = Pet::findOrFail($id);

        $data = [
            'name' => $pet->name ,
            'pol' => $pet->pol ,
            'type' => $pet->type->name,
            'description' =>  $pet->description
        ];

        $pdf = PDF::loadView('pets.pdf', $data);


        return $pdf->stream('pet.pdf');

     //   return $pdf->download('pet.pdf');
    }
}