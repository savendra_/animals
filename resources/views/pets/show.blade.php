@extends('layouts.page')

@section('content')
    <div class="py-5">
        <div class="card py-5 px-5">
            <div class="card-body">
               <div class="">
                   <h5 class="card-title mb-4">{{ $pet->name }}</h5>
                   <p class="card-text">
                       <b>Тип:</b> {{ $pet->type->name }} <br>
                       <b>Пол:</b> @if ($pet->pol=='F') Девочка @else Мальчик @endif <br>
                       {!! nl2br(e($pet->description)) !!}
                   </p>
               </div>
                <div class="mt-5 pt-5">
                    <a href="{{route('pets.index')}}"><i class="las la-arrow-circle-left"></i>Вернуться назад</a>
                </div>
            </div>

        </div>
    </div>
@endsection