<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Pet;

class Type extends Model
{
    use  HasFactory ;


    protected $fillable = [
        'name',
    ];


    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function pets()
    {
        return $this->hasMany(Pet::class);
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($type) {
            $type->pets()->delete();

        });
    }
}
